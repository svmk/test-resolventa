<?php
return [
	'class' => 'app\\video\\VideoProvider',
	'providers' => [
		[
			'class' => 'app\\video\\providers\\youtube\\Youtube',
			'apiKey' => 'AIzaSyDDefsgXEZu57wYgABF7xEURClu4UAzyB8',
			'urlParsers' => [
				[
	                'class' => 'app\video\providers\youtube\parsers\url\Page',
	            ],
	            [
	                'class' => 'app\video\providers\youtube\parsers\url\ShortUrl',
	            ],
	            [
	                'class' => 'app\video\providers\youtube\parsers\url\Embed',
	            ],
			],
			'htmlParsers' => [
				[
	                'class' => 'app\video\parsers\Iframe',
	            ],
			],
		],
		[
			'class' => 'app\\video\\providers\\vimeo\\Vimeo',
			'urlParsers' => [
				[
	                'class' => 'app\video\providers\vimeo\parsers\url\Player',
	            ],            
	            [
	                'class' => 'app\video\providers\vimeo\parsers\url\Page',
	            ],
			],
			'htmlParsers' => [
				[
	                'class' => 'app\video\parsers\Iframe',
	            ],
			],
		],
	],	
];