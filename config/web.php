<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','rest-client',],
    'modules' => [
        'rest-client' => [
            'class' => 'zhuravljov\yii\rest\Module',
            'baseUrl' => 'http://localhost/',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'GRXMxCMhSfYDa9JHBndw2ZLlm2u_bPJR',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),        
        'videoProvider' => require(__DIR__ . '/video_provider.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'GET,HEAD /' => 'site/index',
                'GET,HEAD /rest-client' => 'rest-client',
                'GET,HEAD api/v1/parse/url' => 'parse/url',
                'GET,HEAD api/v1/parse/url.<_format>' => 'parse/url',
                'GET,HEAD api/v1/parse/html.<_format>' => 'parse/html',
                'GET,HEAD api/v1/parse/html' => 'parse/html',
                [
                    'class' => 'yii\rest\UrlRule', 
                    'prefix' => 'api/v1',
                    'controller' => 'video',
                    'tokens' => [
                        '{identity}' => '<identity:.{1,}>',
                        '{service}' => '<service:.{1,}>',
                    ],
                    'patterns' => [
                        'GET,HEAD {service}/{identity}.<_format>' => 'view',
                        'GET,HEAD {service}/{identity}' => 'view',
                    ],
                ],
            ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
