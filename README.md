Тестовое задание Резольвента
============================
Тестовое задание выполнено на базе [Yii-фреймворка](http://www.yiiframework.com/) второй версии.

Структура каталогов
-------------------
    config/video_provider.php               содержит настройки парсеров
    config/web.php                          содержит настройки маршрутизации и компонентов
    controllers/ParseController.php         содержит контроллер парсера
    controllers/VideoController.php         содержит контроллер парсера            
    video/                                  содержит парсеры и провайдеры данных
    tests/codeception/unit/models/VideoProviderTest.php   unit-тесты      

Требования перед установкой
---------------------------

* PHP 5.4


Установка
---------

### Установите зависимости используя composer

Если у вас нет [Composer](http://getcomposer.org/), вы должны установить его [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

После чего выполните команды из корневого каталога проекта

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar install --no-dev
~~~

### Опубликуйте каталог web\ в Apache
Убедитесь, что .htaccess работает для apache. 
Этот проект требует использования маршрутизации до index.php.

Архитектура проекта
-------------------
Для добавления новых парсеров или провайдеров модифицируйте файл ```config/video_provider.php```
