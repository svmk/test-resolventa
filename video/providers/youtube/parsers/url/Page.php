<?php
namespace app\video\providers\youtube\parsers\url;
use app\video\parsers\UrlParser;
use app\video\VideoIdentity;
use webignition\Url\Url;
use app\video\providers\AbstractVideoProvider;
class Page extends UrlParser {
	/**
     * parseUrl парсит URL видео
     * 
     * @param AbstractVideoProvider $provider провайдер
     * @param string $url URL
     *
     * @return VideoIdentity
     */
	public function parseUrl(AbstractVideoProvider $provider,$url) {
		$result = null;
		$url = new Url($url);
		$host = $url->getHost();
		if ($host) {			
			$host = implode('.',array_slice($host->getParts(),-2));
			if ($host == 'youtube.com') {
				$query = $url->getQuery();
				if ($query) {				
					$queryParis = $query->pairs();
					if (isset($queryParis['v']) && is_string($queryParis['v'])) {
						return new VideoIdentity($provider,$queryParis['v']);
					}
				}
			}
		}
		return $result;
	}
}