<?php
namespace app\video\providers\youtube\parsers\url;
use app\video\parsers\UrlParser;
use app\video\VideoIdentity;
use webignition\Url\Url;
use app\video\providers\AbstractVideoProvider;
class ShortUrl extends UrlParser {
	/**
     * parseUrl парсит URL видео
     * 
     * @param AbstractVideoProvider $provider провайдер
     * @param string $url URL
     *
     * @return VideoIdentity
     */
	public function parseUrl(AbstractVideoProvider $provider,$url) {
		$result = null;
		$url = new Url($url);
		$host = $url->getHost();
		if ($host) {			
			$host = implode('.',array_slice($host->getParts(),-2));
			if ($host == 'youtu.be') {
				$hash = ltrim($url->getPath(),'/');
				if ($hash) {
					return new VideoIdentity($provider,$hash);
				}
			}
		}
		return $result;
	}
}