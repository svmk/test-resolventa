<?php
namespace app\video\providers\youtube;
use app\video\providers\AbstractVideoProvider;
use app\video\VideoIdentity;
use yii\base\InvalidConfigException;
use app\video\VideoData;
class Youtube extends AbstractVideoProvider {
    /**
     * $apiKey ключ доступа
     * @var string
     */
	public $apiKey;

    /**
     * @inheritdoc
     */
	public function getService() {
		return 'youtube';
	}

	/**
     * loadVideoData загружает даненые видео
     * @param VideoIdentity $identity идентификатор видео
     * @return VideoData|null
     */
     public function loadVideoData(VideoIdentity $identity) {
        $api = new \Madcoda\Youtube(['key' => $this->apiKey,]);
        $videoInfo = $api->getVideoInfo($identity->getIdentity());
        $result = null;
        if ($videoInfo) {
            $result = new VideoData($identity);    
            $result->title = $videoInfo->snippet->title;
            $result->description = $videoInfo->snippet->description;
            $result->embedHtml = $videoInfo->player->embedHtml;
        }
        return $result;
     }

    /**
     * @inheritdoc
     */
	public function init()
    {
    	if ($this->apiKey === null) {
    		throw new InvalidConfigException('Необходимо заполнить поле apiKey');
    	}
    }
}