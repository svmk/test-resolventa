<?php
namespace app\video\providers;
use yii\base\Component;
use app\video\parsers\UrlParser;
use app\video\parsers\HtmlParser;
use yii\base\InvalidConfigException;
use app\video\VideoIdentity;
use Yii;
abstract class AbstractVideoProvider extends Component {
    /**
     * $urlParsers возвращает парсеры URL-ов
     * @var array
     */
     public $urlParsers = [];

    /**
     * $htmlParsers возвращает парсеры HTML-кодов
     * @var array
     */
     public $htmlParsers = [];

    /**
     * getService возвращает название сервиса
     * 
     * @return string
     */
	abstract public function getService();

    /**
     * loadVideoData загружает даненые видео
     * @param VideoIdentity $identity идентификатор видео
     * @return VideoData|null
     */
     abstract public function loadVideoData(VideoIdentity $identity);

    /**
     * getUrlParsers возвращает парсеры
     * 
     * @return UrlParser[]
     */
     private function getUrlParsers() {
          $result = [];
          foreach ($this->urlParsers as $parser) {
               $parser = Yii::createObject($parser);
               if ($parser instanceof UrlParser) {
                    $result[] = $parser;
               } else {
                    throw new InvalidConfigException(
                         'Класс '.get_class($parser).' не реализовал интерфейс UrlParser'
                    );
               }
          }
          return $result;
     }     

	/**
     * parseUrl парсит URL видео
     * 
     * @param string $url URL
     *
     * @return VideoIdentity|null
     */
     public function parseUrl($url) {
          $result = null;
          foreach ($this->getUrlParsers() as $parser) {
               $result = $parser->parseUrl($this,$url);
               if ($result) {
                    break;
               }
          }
          return $result;             
     }

     /**
     * getHtmlParsers возвращает парсеры
     * 
     * @return HtmlParser[]
     */
     private function getHtmlParsers() {
          $result = [];
          foreach ($this->htmlParsers as $parser) {
               $parser = Yii::createObject($parser);
               if ($parser instanceof HtmlParser) {
                    $result[] = $parser;
               } else {
                    throw new InvalidConfigException(
                         'Класс '.get_class($parser).' не реализовал интерфейс HtmlParser'
                    );
               }
          }
          return $result;
     }

    /**
     * parseHtml парсит HTML-код
     * 
     * @param string $html HTMl-код
     *
     * @return VideoIdentity|null
     */
     public function parseHtml($html) {
          $result = null;
          foreach ($this->getHtmlParsers() as $parser) {
               $result = $parser->parseHtml($this,$html);
               if ($result) {
                    break;
               }
          }
          return $result;  
     }
}