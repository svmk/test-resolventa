<?php
namespace app\video\providers\vimeo;
use app\video\providers\AbstractVideoProvider;
use app\video\VideoIdentity;
use yii\base\InvalidConfigException;
use app\video\VideoData;
class Vimeo extends AbstractVideoProvider {

    /**
     * @inheritdoc
     */
	public function getService() {
		return 'vimeo';
	}

	/**
     * loadVideoData загружает даненые видео
     * @param VideoIdentity $identity идентификатор видео
     * @return VideoData|null
     */
     public function loadVideoData(VideoIdentity $identity) {
        $result = null;
        $url = sprintf(
            'https://vimeo.com/api/oembed.json?url=https://vimeo.com/%s',
            $identity->getIdentity()
        );
        $data = @file_get_contents($url);
        if ($data) {
            $data = @json_decode($data,true);
            if ($data) {
                $result = new VideoData($identity);
                if (isset($data['title'])) {
                    $result->title = $data['title'];
                }
                if (isset($data['description'])) {
                    $result->description = $data['description'];
                }
                if (isset($data['html'])) {
                    $result->embedHtml = $data['html'];
                }
            }
        }
        return $result;
     }
}