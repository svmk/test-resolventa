<?php
namespace app\video\providers\vimeo\parsers\url;
use app\video\parsers\UrlParser;
use app\video\VideoIdentity;
use webignition\Url\Url;
use app\video\providers\AbstractVideoProvider;
class Player extends UrlParser {
	/**
     * parseUrl парсит URL видео
     * 
     * @param AbstractVideoProvider $provider провайдер
     * @param string $url URL
     *
     * @return VideoIdentity
     */
	public function parseUrl(AbstractVideoProvider $provider,$url) {
		$result = null;
		$url = new Url($url);
		$host = $url->getHost();
		if ($host) {						
			if ($host == 'player.vimeo.com') {
				$path = $url->getPath();
				if ($path) {
					$parts = explode('/',ltrim($path,'/'));
					if (count($parts) >= 2) {
						if ($parts[0] == 'video') {
							return new VideoIdentity($provider,$parts[1]);
						}
					}
				}
			}
		}
		return $result;
	}
}