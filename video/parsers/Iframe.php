<?php
namespace app\video\parsers;
use app\video\parsers\HtmlParser;
use app\video\VideoIdentity;
use webignition\Url\Url;
use app\video\providers\AbstractVideoProvider;
use DOMDocument;
use DOMElement;
class Iframe extends HtmlParser {
    /**
     * @inheritdoc
     */
	public function parseHtml(AbstractVideoProvider $provider,$html) {
		$result = null;
		$document = new DOMDocument;
		if ($html) {			
			if (@$document->loadHtml($html)) {
				$iframes = $document->getElementsByTagName('iframe');
				for ($i = 0; $i < $iframes->length; $i++) {
					$iframe = $iframes->item($i);
					if ($iframe instanceof DOMElement) {
						$url = $iframe->getAttribute('src');
						if ($url) {
							$result = $provider->parseUrl($url);
							if ($result) {
								return $result;
							}
						}
					}
				}
			}
		}
		return $result;
	}
}