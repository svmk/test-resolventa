<?php
namespace app\video\parsers;
use app\video\providers\AbstractVideoProvider;
use yii\base\Object;
abstract class HtmlParser extends Object {
    /**
     * parseHtml парсит HTML-код видео
     * 
     * @param AbstractVideoProvider $provider провайдер
     * @param string $html html
     *
     * @return VideoIdentity
     */
	abstract public function parseHtml(AbstractVideoProvider $provider, $html);
}