<?php
namespace app\video\parsers;
use app\video\providers\AbstractVideoProvider;
use yii\base\Object;
abstract class UrlParser extends Object {
    /**
     * parseUrl парсит URL видео
     * 
     * @param AbstractVideoProvider $provider провайдер
     * @param string $url URL
     *
     * @return VideoIdentity
     */
	abstract public function parseUrl(AbstractVideoProvider $provider,$url);
}