<?php
namespace app\video;
use yii\base\Component;
use yii\base\InvalidConfigException;
use Yii;
/**
* VideoProvider предоставляет информацию о видео
* @uses     Component
*/
class VideoProvider extends Component {
    /**
     * $providers предоставляет данные
     * @var AbstractVideoProvider[]
     */
    public $providers = [];

    /**
     * getProviders возвращает провайдеры
     * 
     * @return providers\AbstractVideoProvider[]
     */
    protected function getProviders() {
    	$result = [];
    	foreach ($this->providers as $provider) {
    		$provider = Yii::createObject($provider);
    		if ($provider instanceof providers\AbstractVideoProvider) {
    			$result[] = $provider;
    		} else {
    			throw new InvalidConfigException(
    				'Класс '.get_class($provider).' должен быть наследован от providers\AbstractVideoProvider'
				);
    		}
    	}
    	return $result;
    }

    /**
     * parseUrl парсит URL
     * 
     * @param string $url URL
     *
     * @return VideoIdentity|null
     */
	public function parseUrl($url) {
		$result = null;
		foreach ($this->getProviders() as $provider) {
			$result = $provider->parseUrl($url);
			if ($result) {
				break;
			}
		}
		return $result;
	}

    /**
     * parseHtml парсит HTML 
     * 
     * @param string $html html-код
     *
     * @return VideoIdentity|null
     */
	public function parseHtml($html) {
		$result = null;
		foreach ($this->getProviders() as $provider) {
			$result = $provider->parseHtml($html);
			if ($result) {
				break;
			}
		}
		return $result;		
	}

    /**
     * loadIdentity загружает идентификатор
     * 
     * @param string $service  сервис
     * @param string $identity идентификатор
     *
     * @return VideoIdentity|null
     */
    public function loadIdentity($service,$identity) {
        $result = null;
        foreach ($this->getProviders() as $provider) {
            if ($provider->getService() == $service) {
                $result = new VideoIdentity($provider,$identity);
                break;
            }
        }
        return $result;     
    }
}