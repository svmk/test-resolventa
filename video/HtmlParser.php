<?php
namespace app\video\parsers;
interface HtmlParser {
    /**
     * parseUrl парсит URL видео
     * 
     * @param string $html HTML-код
     *
     * @return VideoIdentity
     */
	public function parseHtml($html);
}