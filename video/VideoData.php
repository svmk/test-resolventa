<?php 
namespace app\video;
class VideoData {
    /**
     * $identity идентификтор 
     * @var string
     */
	public $identity;

    /**
     * $service сервис
     * @var string
     */
    public $service;

    /**
     * $title заголовок 
     * @var string
     */
	public $title;

    /**
     * $description описание
     * @var string
     */
	public $description;

    /**
     * $embedHtml встроенный HTML-код
     * @var string
     */
	public $embedHtml;

    /**
     * asRest возвращает результат для Rest ответа
     * 
     * @return array
     */
    public function asRest() {
        return [
            'service'  => $this->service,
            'identity' => $this->identity,
            'title' => $this->title,
            'description' => $this->description,
            'embedHtml' => $this->embedHtml,
        ];
    }

    /**
     * __construct создаёт запись
     * 
     * @param string $identity идентификтор
     *
     */
	function __construct(VideoIdentity $identity) {
		$this->identity = $identity->getIdentity();
        $this->service  = $identity->getService();
	}
}