<?php
namespace app\video;
use app\video\providers\AbstractVideoProvider;
class VideoIdentity {
    /**
     * $provider
     * @var string
     */
	protected $provider;

    /**
     * $identity идентификатор записи
     * @var string
     */
	protected $identity;

    /**
     * getIdentity возвращает идентификатор
     * 
     * @return string
     */
    public function getIdentity() {
        return $this->identity;
    }

    /**
     * getService возвращает сервис
     * 
     * @return string
     */
    public function getService() {
        return $this->provider->getService();
    }

    /**
     * asRest возвращает данные для Rest ответа
     * 
     * @return array
     */
    public function asRest() {
        return [
            'service' => $this->getService(),
            'identity' => $this->getIdentity(),
        ];
    }

    /**
     * getVideoData возвращает данные
     * 
     * @return VideoData|null
     */
    public function getVideoData() {
        return $this->provider->loadVideoData($this);
    }

    /**
     * __construct создаёт объект идентификатор видео
     * 
     * @param AbstractVideoProvider $provider  сервис
     * @param string $identity идентификатор
     *
     */
	function __construct(AbstractVideoProvider $provider,$identity) {
		$this->provider = $provider;
		$this->identity = $identity;
	}
}