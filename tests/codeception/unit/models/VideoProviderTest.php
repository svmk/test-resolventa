<?php

namespace tests\codeception\unit\models;

use Yii;
use yii\codeception\TestCase;
use app\video\providers\youtube\Youtube;
use app\video\providers\vimeo\Vimeo;
use app\video\VideoIdentity;

class VideoProviderTest extends TestCase
{

    public function testVimeo() {
        $vimeo = new Vimeo;
        $vimeo->loadVideoData(new VideoIdentity($vimeo,'170866054'));
        $vimeo->urlParsers = [
            [
                'class' => 'app\video\providers\vimeo\parsers\url\Player',
            ],            
            [
                'class' => 'app\video\providers\vimeo\parsers\url\Page',
            ],
        ];
        $vimeo->htmlParsers = [
            [
                'class' => 'app\\video\\parsers\\Iframe',
            ],
        ];
        $goodIdentity = new VideoIdentity($vimeo,'170866054');
        $this->assertEquals(
            $goodIdentity,
            $vimeo->parseUrl('https://player.vimeo.com/video/170866054')
        );
        $this->assertEquals(
            $goodIdentity,
            $vimeo->parseUrl('https://vimeo.com/170866054')
        );        
    }

    public function testYouTube()
    {
        $youtube = new Youtube([
            'apiKey' => 'AIzaSyDDefsgXEZu57wYgABF7xEURClu4UAzyB8',
        ]);
        $youtube->urlParsers = [
            [
                'class' => 'app\video\providers\youtube\parsers\url\Page',
            ],
            [
                'class' => 'app\video\providers\youtube\parsers\url\ShortUrl',
            ],
            [
                'class' => 'app\video\providers\youtube\parsers\url\Embed',
            ],
        ];
        $youtube->htmlParsers = [
            [
                'class' => 'app\\video\\parsers\\Iframe',
            ],
        ];
        $this->assertEquals(
            new VideoIdentity($youtube,'ZzYGcJptAxs'),
            $youtube->parseUrl('https://www.youtube.com/watch?v=ZzYGcJptAxs')
        );
        $this->assertNotEquals(
            new VideoIdentity($youtube,'ZzYGcJptAxs1'),
            $youtube->parseUrl('https://www.youtube.com/watch?v=ZzYGcJptAxs')
        );
        $this->assertEquals(
            new VideoIdentity($youtube,'ZzYGcJptAxs'),
            $youtube->parseUrl('https://youtu.be/ZzYGcJptAxs')
        );
        $this->assertNotEquals(
            new VideoIdentity($youtube,'ZzYGcJptAxs1'),
            $youtube->parseUrl('https://youtu.be/ZzYGcJptAxs')
        );
        $this->assertEquals(
            new VideoIdentity($youtube,'ZzYGcJptAxs'),
            $youtube->parseUrl('https://www.youtube.com/embed/ZzYGcJptAxs')
        );
        $this->assertNotEquals(
            new VideoIdentity($youtube,'ZzYGcJptAxs1'),
            $youtube->parseUrl('https://www.youtube.com/embed/ZzYGcJptAxs')
        );

        $this->assertEquals(
            new VideoIdentity($youtube,'ZzYGcJptAxs'),
            $youtube->parseHtml('<iframe width="560" height="315" src="https://www.youtube.com/embed/ZzYGcJptAxs" frameborder="0" allowfullscreen></iframe>')
        );
        $this->assertNotEquals(
            new VideoIdentity($youtube,'ZzYGcJptAxs1'),
            $youtube->parseHtml('<iframe width="560" height="315" src="https://www.youtube.com/embed/ZzYGcJptAxs" frameborder="0" allowfullscreen></iframe>')
        );
        $this->assertEquals(
            null,
            $youtube->parseHtml('')
        );
        $this->assertEquals(
            null,
            $youtube->parseUrl('')
        );
    }

    public function testParser() {
        $video = Yii::$app->videoProvider->parseUrl(
            'https://www.youtube.com/watch?v=ZzYGcJptAxs'
        );
        $this->assertNotNull($video);
        $this->assertEquals(
            'youtube',
            $video->getService()
        );
        $this->assertEquals(
            'ZzYGcJptAxs',
            $video->getIdentity()
        );

        $video = Yii::$app->videoProvider->parseHtml(
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/ZzYGcJptAxs" frameborder="0" allowfullscreen></iframe>'
        );
        $this->assertNotNull($video);
        $this->assertEquals(
            'youtube',
            $video->getService()
        );
        $this->assertEquals(
            'ZzYGcJptAxs',
            $video->getIdentity()
        );
    }

}
