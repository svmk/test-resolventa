<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Тестовое Резольвента';
?>
<div class="site-index">
    <?php 
        $urls = [
            [
                'parse/html',
                'html' => '<iframe width="560" height="315" src="https://www.youtube.com/embed/ZzYGcJptAxs" frameborder="0" allowfullscreen></iframe>',
            ],
            [
                'parse/url',
                'url' => 'https://www.youtube.com/watch?v=ZzYGcJptAxs',
            ],
            [
                'parse/url',
                'url' => 'https://vimeo.com/170866054',
            ],
            [
                'video/view',
                'service' => 'youtube',
                'identity' => 'ZzYGcJptAxs',
            ],
            [
                'video/view',
                'service' => 'vimeo',
                'identity' => '170866054',
            ],
        ];
    ?>
    <h1>Список запросов</h1>
    <ul>
        <?php foreach ($urls as $url) { ?>
        <li>
            <?php 
                $url = Url::to($url,true);
                echo Html::a($url,$url);
            ?>            
        </li>
        <?php } ?>
    </ul>
</div>
