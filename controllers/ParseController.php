<?php
namespace app\controllers;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use Yii;
class ParseController extends Controller {

    /**
     * renderResult возвращает результат
     * 
     * @param VideoIdentity $identity идентфиикатор
     * @param boolean $redirect редиректить результат?
     *
     * @return mixed
     */
    protected function renderResult($identity,$redirect) {
        $url = Url::to([
            'video/view',
            'service' => $identity->getService(),
            'identity' => $identity->getIdentity(),
            '_format' => Yii::$app->request->get('_format'),
        ],true);
        if ($redirect) {
            $this->redirect($url,303);        
        } else {
            return ArrayHelper::merge(
                [
                    'url' => $url,
                ],
                $identity->asRest()
            );
        }        
    }
    /**
     * actionHtml парсит HTML-данные
     * 
     * @param string $html контент
     * @param boolean $redirect делать редирект
     */
    public function actionHtml($html,$redirect = false) {
        $identity = Yii::$app->videoProvider->parseHtml(
            $html
        );
        if ($identity === null) {
            throw new NotFoundHttpException('Не удалось распарсить HTML');
        }
        return $this->renderResult($identity,$redirect);
	}

    /**
     * actionUrl парсит URL 
     * 
     * @param string $url URl
     * @param boolean $redirect делать редирект
     *
     */
    public function actionUrl($url,$redirect = false) {
        $identity = Yii::$app->videoProvider->parseUrl(
            $url
        );
        if ($identity === null) {
            throw new NotFoundHttpException('Не удалось распарсить URL');
        }
        return $this->renderResult($identity,$redirect);
    }

}