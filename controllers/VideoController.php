<?php
namespace app\controllers;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use Yii;
class VideoController extends Controller {
    /**
     * actionView возвращает информацию
     * 
     * @param string $service  сервис
     * @param string $identity идентификатор
     *
     * @return array
     */
    public function actionView($service,$identity) {
        $video = Yii::$app->videoProvider->loadIdentity(
            $service,
            $identity
        );
        if ($video === null) {
            throw new NotFoundHttpException('Не удалось загрузить видео');
        }
        $videoData = $video->getVideoData();
        if ($videoData === null) {
            throw new NotFoundHttpException('Видео не найдено');   
        }
        return $videoData->asRest();
    }
}